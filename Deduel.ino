
#include <AccelStepper.h>
#include <AFMotor.h>

int pin_LED_G = 1;
int pin_LED_B = 2;
int pin_LED_R = 0;
int pin_tone = 13;
int pin_bouton_limite_1 = 11;
int pin_bouton_limite_2 = 10;
int pin_bouton_joueur_1 = 3;
int pin_bouton_joueur_2 = 9;
int longueur_rail;
int increment_base = 40;
int joueur_present_1 = 0;
int joueur_present_2 = 0;
int jeu = 101;
int duree;
int temps_de_jeu;
int increment;
int buffer_joueur_1;
int buffer_joueur_2;
int etat_joueur_1;
int etat_joueur_2;
int gagnant_declare;
int buffer_jeu;
int temps_de_jeu_total = 0;
int buffer_temps_de_jeu_total = 0;
int temps_de_jeu_interm;
long Lim_plus;
long Lim_moins;
long initialPosition;

// two stepper motors one on each port
AF_Stepper motor2(200, 2);

// you can change these to DOUBLE or INTERLEAVE or MICROSTEP!

// wrappers for the second motor!
void forwardstep2() {  
  motor2.onestep(FORWARD, SINGLE);
}
void backwardstep2() {  
  motor2.onestep(BACKWARD, SINGLE);
}

int moveMultistep2(int increment, AccelStepper stepper2) {
  initialPosition = stepper2.currentPosition();
  if (increment < 0) {

      while (stepper2.currentPosition() > initialPosition + increment){
      if (digitalRead(pin_bouton_limite_2) == LOW and digitalRead(pin_bouton_limite_1) == LOW) { //Limite 1 devrait toujours etre LOW
        stepper2.moveTo(stepper2.currentPosition()-10);
        while(stepper2.distanceToGo() !=0 ){
            stepper2.run();
        }
      }
      else {

        goto Joueur_2_gagne;
      }
    }
  }
  else {
   while (stepper2.currentPosition() < initialPosition + increment){
      if (digitalRead(pin_bouton_limite_1) == LOW and digitalRead(pin_bouton_limite_2) == LOW) { //Limite 2 devrait toujours etre LOW
        stepper2.moveTo(stepper2.currentPosition()+10);
        while(stepper2.distanceToGo() !=0 ){
            stepper2.run();
        }
      }
      else {

        goto Joueur_1_gagne;
      }
    }
  }
  return 0;
  
  Joueur_1_gagne:
    for (int i = 100; i < 8000 ; i = ceil(i * 2)) {
      tone(pin_tone, i, 50);
      delay(100);
    }
    tone(pin_tone, 8000, 1500);
    delay(1900);
    stepper2.moveTo(stepper2.currentPosition()-700);
    while (  stepper2.distanceToGo() != 0) {
      stepper2.run();
    }
    return 1;
    

  Joueur_2_gagne:
    for (int i = 8000; i > 100 ; i = floor(i * 0.5)) {
      tone(pin_tone, i, 50);
      delay(100);
    }
    delay(150);
    tone(pin_tone, 2000, 50);
    delay(100);
    tone(pin_tone, 2000, 1500);
    delay(1900);
    stepper2.moveTo(stepper2.currentPosition()+700);
    while (  stepper2.distanceToGo() != 0) {
      stepper2.run();
    }
    return 1;
}

long trouveLeMilieu(AccelStepper stepper2) {

  // Calibration longueur_rail, DEL bleue, DEL rouge, DEL vert, bip
  digitalWrite(pin_LED_R, HIGH);
  digitalWrite(pin_LED_G, HIGH);
  digitalWrite(pin_LED_B, HIGH);

  tone(pin_tone, 1000, 500);

  longueur_rail = 0;
  
  while(digitalRead(pin_bouton_limite_1) == LOW){
    stepper2.moveTo(stepper2.currentPosition()+20);
    while(stepper2.distanceToGo() != 0) {
      stepper2.run();
    }

  }
  
  Lim_plus = stepper2.currentPosition();
  tone(pin_tone, 1000, 500);
  
  while(digitalRead(pin_bouton_limite_2) == LOW){
    stepper2.moveTo(stepper2.currentPosition()-20);
    while(stepper2.distanceToGo() != 0) {
      stepper2.run();
    }
  }
  
  Lim_moins = stepper2.currentPosition();
  
//  stepper2.moveTo(stepper2.currentPosition()+40);
//  while (digitalRead(pin_bouton_limite_1) == 0) {
//    stepper2.moveTo(stepper2.currentPosition()+40);
//    stepper2.run();
//  }
//  Lim_plus = stepper2.currentPosition();
//  tone(pin_tone, 1000, 500);
//  while (digitalRead(pin_bouton_limite_2) == 0) {
//    stepper2.moveTo(stepper2.currentPosition()-40);
//    stepper2.run();
//    longueur_rail++;
//  }
//  Lim_moins = stepper2.currentPosition();
//  // Increment initial = 1% du rail
//  increment = ceil(longueur_rail / 100);

  // Retour au centre, bip bip
  tone(pin_tone, 1500, 300);
  delay(200);
  tone(pin_tone, 2000, 300);
  
  return (floor((Lim_moins + Lim_plus)/2));
}

void vaAuMilieu(AccelStepper stepper2, long positionMilieu) {

  digitalWrite(pin_LED_R, HIGH);
  digitalWrite(pin_LED_G, HIGH);
  digitalWrite(pin_LED_B, HIGH);
//  if (stepper2.currentPosition() < positionMilieu) {
//    while (stepper2.currentPosition() < positionMilieu) {
//        stepper2.moveTo(stepper2.currentPosition()+40);
//        stepper2.run();
//    }
//  }
//  if (stepper2.currentPosition() < positionMilieu) {
//    while (stepper2.currentPosition() < positionMilieu) {
//        stepper2.moveTo(stepper2.currentPosition()+40);
//        stepper2.run();
//    }
//  }
//  if (positionMilieu < stepper2.currentPosition()) {
//    stepper2.moveTo(stepper2.currentPosition()-40);
//    stepper2.run();
//  }
//  else {
//    stepper2.moveTo(stepper2.currentPosition()+40);
//    stepper2.run();
//  }
  
  stepper2.moveTo(positionMilieu);
  while(stepper2.distanceToGo() != 0) {
//    if (digitalRead(pin_bouton_limite_1) == HIGH || digitalRead(pin_bouton_limite_2) == HIGH){
//      break;
//    }
    stepper2.run();
  }
  
  digitalWrite(pin_LED_R, LOW);
  digitalWrite(pin_LED_G, LOW);
  digitalWrite(pin_LED_B, LOW);
  delay(1000);

  // Ready to go, attend que les deux joueurs pesassent sur leur bouton respectif
  do {
    if (digitalRead(pin_bouton_joueur_1) == HIGH) {
      joueur_present_1 = 1;
      tone(pin_tone, 2000, 100);
    }

    if (digitalRead(pin_bouton_joueur_2) == HIGH) {
      joueur_present_2 = 1;
      tone(pin_tone, 1000, 80);
    }
  }
  while (joueur_present_1 == 0 || joueur_present_2 == 0);

  joueur_present_1 = 0;
  joueur_present_2 = 0;
  delay(1000);
  // clignote 5 fois, bip bip bip bip bip

  for (int i = 0 ; i < 5 ; i++) {
    tone(pin_tone, 1000, 300);
    digitalWrite(pin_LED_R, HIGH);
    digitalWrite(pin_LED_G, HIGH);
    digitalWrite(pin_LED_B, HIGH);
    delay(500);
    digitalWrite(pin_LED_R, LOW);
    digitalWrite(pin_LED_G, LOW);
    digitalWrite(pin_LED_B, LOW);
    delay(500);
  }
  return;
}

// Motor shield has two motor ports, now we'll wrap them in an AccelStepper object
AccelStepper stepper2(forwardstep2, backwardstep2);

void setup() {
  stepper2.setMaxSpeed(500.0);
  stepper2.setAcceleration(1000.0);
 
  pinMode(pin_LED_R, OUTPUT);
  pinMode(pin_LED_G, OUTPUT);
  pinMode(pin_LED_B, OUTPUT);
  
  pinMode(pin_bouton_limite_1, INPUT);
  pinMode(pin_bouton_limite_2, INPUT);
  pinMode(pin_bouton_joueur_1, INPUT);
  pinMode(pin_bouton_joueur_2, INPUT);
 
}

void loop() {

  
//stepper2.run();
//long positionMilieu = trouveLeMilieu(stepper2);
//vaAuMilieu(stepper2,positionMilieu);

stepper2.moveTo(-700);
while (  stepper2.distanceToGo() != 0) {
  stepper2.run();
}

start:
  

  // Incrementation du niveau de difficulte
//  if (temps_de_jeu_total > 90 and buffer_temps_de_jeu_total < 90) {
//    increment_base = increment_base * 2;
//  }
//  else if (temps_de_jeu_total > 60 and buffer_temps_de_jeu_total < 60) {
//    increment_base = increment_base * 2;
//  }
//  else if (temps_de_jeu_total > 30 and buffer_temps_de_jeu_total < 30) {
//    increment_base = increment_base * 2;
//  }
  
  buffer_temps_de_jeu_total = temps_de_jeu_total;
  delay(2000);
  // REPARTITEUR
  buffer_jeu = jeu;
  digitalWrite(pin_LED_R, LOW);
  digitalWrite(pin_LED_G, LOW);
  digitalWrite(pin_LED_B, LOW);
  //while (1) {
    jeu = random(100);

    if (jeu < 45 && buffer_jeu != 1) {
      buffer_jeu = 1;
      tone(pin_tone, 1000 + temps_de_jeu_total * 15, 300);
      goto PPV;//PPV; // Pese plus vite
    }
    else if (jeu < 80 && buffer_jeu != 2) { // Suit le motif
      buffer_jeu = 2;
      tone(pin_tone, 1000 + temps_de_jeu_total * 15, 300);
      goto SLM;//SLM;
    }
    else if (buffer_jeu != 3) { // Ping Pong
      buffer_jeu = 3;
      tone(pin_tone, 1000 + temps_de_jeu_total * 15, 300);
      goto  SLM;//PIPO;
    }
 // }

PPV :
  digitalWrite(pin_LED_B, HIGH);

  duree = 0;
  buffer_joueur_1 = 0;
  buffer_joueur_2 = 0;

  temps_de_jeu = 200 + random(300);
  temps_de_jeu_total += temps_de_jeu;

  do {
    increment = 0;
    if (digitalRead(pin_bouton_joueur_1) == HIGH) {
      etat_joueur_1 = 1;
    }
    else {
      etat_joueur_1 = 0;
    }

    if (digitalRead(pin_bouton_joueur_2) == HIGH) {
      etat_joueur_2 = 1;
    }
    else {
      etat_joueur_2 = 0;
    }
    increment = ((etat_joueur_2 xor buffer_joueur_2) - (etat_joueur_1 xor buffer_joueur_1))*1;
//    if (etat_joueur_1 xor buffer_joueur_1) {
//      increment += increment_base;
//    }
//    if (etat_joueur_2 == 1 && buffer_joueur_2 == 0) {
//      increment -= increment_base;
//    }
    
    if (increment != 0) {
      tone(pin_tone, 2000, 100);
      //tone(pin_tone, 1000 + temps_de_jeu_total * 10 + increment, 100);
      //moveMultistep2(increment, stepper2);
      //delay(100);
      duree = duree + 10;
    }

    gagnant_declare = moveMultistep2(increment, stepper2);
	  if (gagnant_declare == 1) {
	    //vaAuMilieu(stepper2, positionMilieu);
      goto start;
	  }
    buffer_joueur_1 = etat_joueur_1;
    buffer_joueur_2 = etat_joueur_2;
    duree = duree + 0.001;
    //delay(100);
  }
  while (duree < temps_de_jeu);

  digitalWrite(pin_LED_B, LOW);

  goto start;

SLM :
  duree = 0;

  temps_de_jeu = 6000 + random(7000);
  temps_de_jeu_total += temps_de_jeu;
  do {
    temps_de_jeu_interm = 5 + random(15);
    digitalWrite(pin_LED_G, HIGH);  //vert
    for (int i = 0 ; i < temps_de_jeu_interm ; i++) {
      if (digitalRead(pin_bouton_joueur_1) == HIGH) {
        increment = -increment_base;
      }
      else {
        increment = 0;
      }

      if (digitalRead(pin_bouton_joueur_2) == HIGH) {
        increment = increment + increment_base;
      }

      if (increment != 0){
        tone(pin_tone, 1000 + temps_de_jeu_total * 10 + increment, 100);
      }
	    if (moveMultistep2(increment, stepper2)) {
        //vaAuMilieu(stepper2, positionMilieu);
	      goto start;
	    }


      delay(100);
      duree = duree + 100;
    }
    temps_de_jeu_interm = 5 + random(15);

    digitalWrite(pin_LED_G, LOW);  //noir
    for (int i = 0 ; i < temps_de_jeu_interm ; i++) {
      if (digitalRead(pin_bouton_joueur_1) == LOW) {
        increment = -increment_base;
      }
      else {
        increment = 0;
      }

      if (digitalRead(pin_bouton_joueur_2) == LOW) {
        increment = increment + increment_base;
      }

      if (increment != 0) {
        tone(pin_tone, 1000 + temps_de_jeu_total * 10 + increment, 100);
      }
	  if (moveMultistep2(increment, stepper2)) {
      //vaAuMilieu(stepper2, positionMilieu);
	    goto start;
	  }

      delay(100);
      duree = duree + 0.1;
    }
  }
  while (duree < temps_de_jeu);

  goto start;
//
//PIPO :
//  duree = 0;
//  buffer_joueur_1 = random(1); // joueur actif: 0=> joueur 1;  1=> joueur 2
//  temps_de_jeu = 3000 + random(4000);
//  temps_de_jeu_total += temps_de_jeu;
//
//  digitalWrite(pin_LED_R, HIGH);
//  if (buffer_joueur_1 == 0) {
//    digitalWrite(pin_LED_B, HIGH); // violet
//  }
//
//  do {
//    if (buffer_joueur_1 == 0) {
//      if (digitalRead(pin_bouton_joueur_1) == LOW) {
//        increment = -increment_base;
//      }
//      else {
//        buffer_joueur_1 = 1; //Ping
//        digitalWrite(pin_LED_B, LOW); // rouge
//        increment = 0;
//      }
//      
//      if (digitalRead(pin_bouton_joueur_2) == HIGH) {
//        increment = increment + increment_base;
//      }
//      else {
//        if (digitalRead(pin_bouton_joueur_2) == LOW) {
//          increment = increment_base;
//        }
//        else {
//          buffer_joueur_1 = 0; //Pong
//          analogWrite(pin_LED_B, 1); // violet
//          increment = 0;
//        }
//        if (digitalRead(pin_bouton_joueur_2) == HIGH) {
//          increment = increment - increment_base;
//        }
//      }
//      if (increment != 0) {
//        tone(pin_tone, 1000 + temps_de_jeu_total * 10 + increment, 100);
//      }
//	  if (moveMultistep2(increment)) {
//	    break;
//	  }
//
//      delay(100);
//      duree = duree + 0.1;
//    }
//    while (duree < temps_de_jeu);
//
//    digitalWrite(pin_LED_R, LOW);
//    digitalWrite(pin_LED_B, LOW); // noir
//    goto start;


  }
