Deduel is the second generation of the Dedu game, a popular drinking game in the Refuges du camping du mont Albert around New Year Day.

Requirements:
Arduino board with Step-motor module. Custom hardware is also necessary.

Set-up:
The game is played with two players, each provided with a hand held device featuring an ergonomic button, connected to the Arduino. Between the two players is a track with a little cart on it, on which a 355-ml beer can can be placed. By playing a variety of games using their button, the players try to make the little cart go to the other player's end of the track.

Games:
   * Game 1: the players use the button as fast as possible, repetitively. Each time they push, the cart moves away.
   * Game 2: the players try to reproduce on the button the pattern shown by a LED. When the button does not match the LED while the other player's one does, the cart moves back.
   * Game 3: Ping pong: the buttons must be pushed alternatively by player 1 and player 2. Cart moves back if the pattern is not respected and away if the payers are too slow to click the button.
   
Important note:
   * The game is too slow, nobody will drink significantly unless you drink the whole beer when you lose. Would require improvement in the control of the step motor and to make the game finish faster. 
   * Developed mainly in a car and then around people partying.